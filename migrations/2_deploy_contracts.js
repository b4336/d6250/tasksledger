// This file does the following: 
// Whenever a new smart contract is created, the state of the blockchain
// is updated. A blockchain is fundamentally a database, hence, whenever
// it is changed or updated (permanently), we must migrate it from one
// state to another state. This is similar to a database migration in 
// other regular web application development frameworks. Note: this file
// as well as the files of other scripts in this migrations directory
// have to be numbered in the file names so that Truffle executes them 
// in the order specified.

const TasksLedger = artifacts.require("./TasksLedger.sol");

module.exports = function (deployer) {
  deployer.deploy(TasksLedger);
};